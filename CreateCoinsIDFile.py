import requests
import json


# Save the json data to file
def save_to_file(data, filename):
    with open(filename, 'w') as f:
        json.dump(data, f, indent=2, ensure_ascii=False)


# Get the amount of crypto currency from coinmarketcap
def get_global_data():
    request = requests.get('https://api.coinmarketcap.com/v2/global/')
    if request.status_code == 200:
        amount = request.json()['data']['active_cryptocurrencies']
        cap = request.json()['data']['quotes']['USD']['total_market_cap']
        volume24_usd = request.json()['data']['quotes']['USD']['total_volume_24h']
        request = requests.get('https://api.coinmarketcap.com/v2/global/?convert=BTC')
        volume24_btc = request.json()['data']['quotes']['BTC']['total_volume_24h']
        return {'amount': amount, 'cap': cap, 'volume24_usd': volume24_usd, 'volume24_btc': volume24_btc}
    else:
        return None


# Create file with coins data = ID + name
def create():
    global_data = get_global_data()
    if global_data is None:
        print("ERROR - can't get the amount of crypto currency")
    else:
        save_to_file(global_data, 'market_global.json')
        amount = get_global_data()['amount']
        i = 0
        coins_array = {}
        with open('market_coins.json', 'a') as f:
            while i < amount:
                request = requests.get('https://api.coinmarketcap.com/v2/ticker/?start=' + str(i + 1) +
                                       '&limit=100&sort=id&structure=array')
                if request.status_code != 200:
                    print("Can't get coin data package for last 100 coins of " + str(i))
                else:
                    print('Success get coins from ' + str(i))
                    for coin in request.json()['data']:
                        coins_array[str.lower(coin['name'])] = coin['id']
                i += 100
            json.dump(coins_array, f, indent=2, ensure_ascii=False)


if __name__ == '__main__':
    create()

