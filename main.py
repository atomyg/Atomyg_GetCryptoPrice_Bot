import requests
import json
import re
from flask import Flask
from flask import request
from flask import jsonify
from flask_sslify import SSLify

app = Flask(__name__)
sslify = SSLify(app)
TOKEN = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
BOT_URL = 'https://api.telegram.org/bot%s/' % TOKEN


# Save json data to file (from GET request)
def save_to_file(data, filename):
    with open(filename, 'w') as f:
        json.dump(data, f, indent=2, ensure_ascii=False)


# Read the json file with a coins ID and name
def read_from_file():
    with open('/home/atomyg/TeleGetCryptoPrice/market_coins.json') as f:
        coins_array = json.load(f)
    return coins_array


# Get Bot status
def get_status():
    request1 = requests.get(BOT_URL + 'GetMe')
    json_data = request1.json()
    save_to_file(json_data, '/home/atomyg/TeleGetCryptoPrice/bot_status.json')


# Send message to Bot in current user's chat
def send_message(chat_id, text):
    answer = {'chat_id': chat_id, 'text': text}
    request1 = requests.post(BOT_URL + 'SendMessage', answer)
    return request1.json()


# Analyse the input message (or command) from Bot
def parse_text(message):
    pattern = r'/\w+' # We are looking for any name of coin
    match = re.search(pattern, message)
    if match:
        command = match.group()
        command = command.replace("_", " ")
        return command[1:]
    return None


# Get coin price from coinmarketcap
def get_token_price(coin_id):
    r = requests.get('https://api.coinmarketcap.com/v2/ticker/' + str(coin_id))
    if r.status_code != 200:
        print("Can't get coi id " + str(coin_id) + 'data from coinmarketcap')
        price = 'Server Error. Try again after 5 minutes please'
    else:
        json_data = r.json()['data']
        price = json_data['quotes']['USD']['price']
    return price


@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        json_data = request.get_json() # Get data from POST request on FLASK (localhost)
        save_to_file(json_data, '/home/atomyg/TeleGetCryptoPrice/bot_webhook.json')
        if 'message' in json_data:
            chat_id = json_data['message']['chat']['id']
            message = json_data['message']['text']
            token = parse_text(message)
            if token is not None:
                coins = read_from_file()
                if token in coins:
                    coin_id = coins[token]
                    price = get_token_price(coin_id)
                    send_message(chat_id, str(price) + ' $')
            else:
                send_message(chat_id, "Can't find this coin name. Need right full coin name.")
            return jsonify(json_data)
    return '<h1>Atomyg_GetCryptoPrice_Bot welcomes you!</h1>'


if __name__ == "__main__":
    app.run()
